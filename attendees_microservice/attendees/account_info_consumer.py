from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()
# need to import after setup because otherwise database isn't setup yet
from attendees.models import AccountVO


def update_AccountVO(ch, method, properites, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.update_or_create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            is_active=is_active,
            updated=updated,
        )
    else:
        AccountVO.objects.delete(email)


#   if is_active:
#       Use the update_or_create method of the AccountVO.objects QuerySet
#           to update or create the AccountVO object
#   otherwise:
#       Delete the AccountVO object with the specified email, if it exists

while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host="rabbitmq")
        )
        channel = connection.channel()

        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )

        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue

        channel.queue_bind(exchange="account_info", queue=queue_name)
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_AccountVO,
            auto_ack=True,
        )

        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)



# from datetime import datetime
# import json
# import pika
# from pika.exceptions import AMQPConnectionError
# import django
# import os
# import sys
# import time


# sys.path.append("")
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
# django.setup()

# from attendees.models import AccountVO


# def update_account(ch, method, properties, body):
#     content = json.loads(body)
#     first_name = content["first_name"]
#     last_name = content["last_name"]
#     email = content["email"]
#     is_active = content["is_active"]
#     updated_string = content["updated"]
#     updated = datetime.fromisoformat(updated_string)
#     if is_active:
#         AccountVO.objects.update_or_create(
#             first_name=first_name,
#             last_name=last_name,
#             email=email,
#             is_active=is_active,
#             updated_string=updated_string,
#             updated=updated,
#         )
#     else:
#         AccountVO.objects.delete_by_email(email)


# while True:
#     try:
#         connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq"))
#         channel = connection.channel()
#         channel.exchange_declare(exchange="account_info", exchange_type="fanout")
#         result = channel.queue_declare(queue="", exclusive=True)
#         queue_name = result.method.queue
#         channel.queue_declare(queue="queue_name")
#         channel.queue_bind(queue="queue_name", exchange="account_info")
#         channel.basic_consume(update_account, queue="queue_name", no_ack=True)
#         channel.start_consuming()
#     except AMQPConnectionError:
#         print("Connection error, retrying in 5 seconds")
#         time.sleep(5)
